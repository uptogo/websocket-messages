import { Message, Route, Stop } from './index'

export interface Read extends Message {
  id: number;
  keys: string[];
  processed: string[];
  route: Route;
  stop?: Stop;
  type: 'READ';
}
