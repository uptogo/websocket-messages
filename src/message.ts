import { Agent } from './index'

export interface Message {
  sender?: Agent;
  subject: 'REQUEST' | 'PROGRESS' | 'END';
  type: 'READ';
}
