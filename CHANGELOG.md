# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [5.0.0] - 2019-10-07

### Removed
- Quantity attribute from Read interface.

### Changed
- Keys attribute required in Read interface.
- Processed attribute required in Read interface.

## [4.0.0] - 2019-10-06

### Changed
- Convert processed attribute to string array.

## [3.1.0] - 2019-10-06

### Added
- Identificator attribute to Agent.

### Changed
- Name optional in Agent.

## [3.0.0] - 2019-10-04

### Changed
- Report reading progress rather than just beginning.

## [2.0.0] - 2019-10-03

### Removed
- Type enumeration.
- Subject enumeration.

### Changed
- Sender optional in Message.

## [1.0.2] - 2019-10-03

### Changed
- Stop optional in Read message.

## [1.0.1] - 2019-10-03

### Removed
- Dependency ```crc-32```.

## [1.0.0] - 2019-10-03

### Added
- Type enumeration.
- Subject enumeration.
- Key Validation interface.
- Stop interface.
- Route interface.
- Message interface.
- Agent interface.
- Project boilerplate.
