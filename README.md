# WebSocket Messages

Accepted messages by the Uptogo WebSocket.

## Installation

Use the package manager [npm](https://docs.npmjs.com/) to install the package.

```bash
npm install @uptogo/websocket-messages
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
