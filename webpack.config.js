const path = require('path')

module.exports = {
  entry: './src/index.ts',
  mode: 'production',
  module: {
    rules: [{
      exclude: [
        /\.test\.ts$/,
        /node_modules/
      ],
      test: /\.ts$/,
      use: [
        'babel-loader',
        'ts-loader'
      ]
    }]
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'lib')
  },
  resolve: {
    extensions: [
      '.ts',
      '.js'
    ]
  }
}
