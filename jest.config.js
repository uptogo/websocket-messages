module.exports = {
  collectCoverage: true,
  roots: [
    '<rootDir>/src'
  ],
  testRegex: '\\.test\\.ts$',
  transform: {
    '\\.ts$': 'ts-jest'
  },
  verbose: true
}
